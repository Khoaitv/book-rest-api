<?php

namespace App\Traits;

use Exception;

trait ApiResponseTrait{
    
    public function successResponse(string $message, $data, int $code = 200) {
        return response()->json([
            'status' => true,
            'code'   => $code,
            'message'=> $message,
            'data'   => $data
        ]);
    }

    public function failResponse(string $message, int $code = 404) {
        return response()->json([
            'status' => false,
            'code'   => $code,
            'message'=> $message,
        ]);
    }
}