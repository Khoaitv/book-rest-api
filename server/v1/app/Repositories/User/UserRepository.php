<?php
namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\Cores\Class\BaseAbstract;

class UserRepository extends BaseAbstract implements UserInterface
{
    public function __construct()
    {
        parent::__construct(new User());
    }
}
