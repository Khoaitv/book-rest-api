<?php
namespace App\Repositories\Cores\Class;

use App\Repositories\Cores\BaseInterface;
use Illuminate\Database\Eloquent\Model;

abstract class BaseAbstract implements BaseInterface {

    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes) {
        return $this->model->create($attributes);
    }

    public function update(int $id, array $attributes)
    {
        return $this->model->updateOrCreate(['id'=> $id], $attributes);
    }

    public function delete(int $id)
    {
        return $this->model->destroy($id);
    }

    public function destroy(array $id)
    {
        return $this->model->destroy($id);
    }
}