<?php
namespace App\Repositories\Cores;

Interface BaseInterface{

    /**
     * * create resource
     */
    function create(array $attributes);

    /**
     * * update resource
     */
    function update(int $id, array $attributes);

    /**
     * * delete a resource
     */
    function delete(int $id);

    /**
     * * delete array resource
     */
    function destroy(array $id);

}