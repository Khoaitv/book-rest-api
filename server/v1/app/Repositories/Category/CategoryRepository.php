<?php
namespace App\Repositories\Category;

use App\Models\Category;
use App\Repositories\Cores\Class\BaseAbstract;

class CategoryRepository extends BaseAbstract implements CategoryInterface {

    public function __construct()
    {
        parent::__construct(new Category());
    }
}