<?php

namespace App\Exceptions;

use App\Traits\ApiResponseTrait;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    use ApiResponseTrait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];


    public function render($request, Throwable $e)
    {
        if ($request->is('api/*')) {

            if ($e instanceof ValidationException) {

                return $this->failResponse($e->getMessage(), 422);
            }

            if($e instanceof ModelNotFoundException) {
                return $this->failResponse('Data Not Found', 404);
            }

            if ($e instanceof NotFoundHttpException) {
                return $this->failResponse('Router Not Found', 404);
            }

            if ($e->getMessage() == "Unauthorized") {
                return $this->failResponse('Unauthorized', 401);
            }

            if ($e instanceof TokenInvalidException) {
                return $this->failResponse('Invalid token', 401);
            }

            if ($e instanceof TokenExpiredException) {
                return $this->failResponse('Token has Expired', 401);
            }

            if ($e instanceof JWTException) {
                return $this->failResponse('Token not parsed', 401);
            }

            if (config('app.debug')) {
				return parent::render($request, $e);
			}
           
			return $this->failResponse('Unexpected exception', 500);
        }

        return parent::render($request, $e); 
    }

}
