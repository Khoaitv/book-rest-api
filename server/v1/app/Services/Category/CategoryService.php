<?php

namespace App\Services\Category;

use App\Repositories\Category\CategoryInterface;
use Illuminate\Support\Str;

class CategoryService{
    protected $categoryRepository;

    public function __construct(CategoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function create(array $attributes) {
        $attributes = $this->convertAttr($attributes, "create");

        return $this->categoryRepository->create($attributes);
    }


    public function update(array $attributes, int $id) {
        $attributes = $this->convertAttr($attributes, "update");
        
        return $this->categoryRepository->update($id, $attributes);
    }

    private function convertAttr($attributes, $action) {
        $result = [
            'name' => $attributes['name'],
            'slug' =>  Str::slug($attributes['name']),
            'status' => ($attributes['status']) ? 1 : 0,
            'parent_id' => $attributes['parent_id']
        ];

        if ("update" == $action) {

            return $result;
        }

        if ("create" == $action) {
            $result["code"] = Str::random(8);

            return $result;
        }
       
    }

    
}