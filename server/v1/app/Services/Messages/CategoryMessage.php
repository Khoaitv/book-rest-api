<?php

namespace App\Services\Messages;

class CategoryMessage{
    const ADD_CATE_SUCCESS = 'Add category success';
    const UPDATE_CATE_SUCCESS = 'Update category success';
    const CATE_NOT_FOUND = "Category not found";
}