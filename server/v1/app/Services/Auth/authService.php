<?php
namespace App\Services\Auth;

use App\Repositories\User\UserInterface;
use App\Services\Messages\AuthMessage;
use App\Traits\ApiResponseTrait;

class authService{
    use ApiResponseTrait;
    protected $userRepository;

    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function userRegister(array $userInfo) {
        $userInfo['password'] = $this->hashPassword($userInfo['password']);

        $user =  $this->userRepository->create($userInfo);
        
       return $this->successResponse(AuthMessage::ADD_AUTH_SUCCESS, $user);
    }

    public function createToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }

    private function hashPassword($password) {
        return bcrypt($password);
    }
    
}