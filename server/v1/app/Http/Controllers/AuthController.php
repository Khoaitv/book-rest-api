<?php

namespace App\Http\Controllers;

use App\Services\Auth\authService;
use Exception;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(authService $authService)
    {
        $this->authService = $authService;
        
    }

    /**
     * user register
     */
    public function register(Request $request) {
        try{

            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
            ]);

            $userInfo = $request->only(['name','email','password']);

            return $this->authService->userRegister($userInfo);

        } catch(\Exception $e) {
            throw $e;
        }

    }

    /**
     * user login
     */
    public function login(Request $request) {
        try{
            $request->validate([
                'email' => 'required|email',
                'password' => 'required|string',
            ]);
            $credentials = $request->only(['email','password']);
            
            if (!$token = auth('api')->attempt($credentials)) {
                throw new Exception("Unauthorized");
            }

            return $this->authService->createToken($token);

        } catch(\Exception $e) {
            throw $e;
        }
    }

    public function logout() {
        auth('api')->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }

    public function refresh() {
        $newToken = auth('api')->refresh();
        return $this->authService->createToken($newToken);
    }

    public function userProfile() {
        return response()->json(auth('api')->user());
    }
}
