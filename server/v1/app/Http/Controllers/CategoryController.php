<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Services\Category\CategoryService;
use App\Services\Messages\CategoryMessage;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validated = $request->validate([
                'name'      => 'required|unique:categories',
                'status'    => 'required',
                'parent_id' => 'required'
            ]);
            

            $category = $this->categoryService->create($validated);
           return $this->successResponse(CategoryMessage::ADD_CATE_SUCCESS, $category, 201);
        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        try{
            if (!$category) {
                return $this->failResponse(CategoryMessage::CATE_NOT_FOUND, 404);
            }

            $validated = $request->validate([
                'name'      => 'required|unique:categories,id,'.$category->id,
                'status'    => 'required',
                'parent_id' => 'required'
            ]);
            

            $category = $this->categoryService->update($validated, $category->id);
           return $this->successResponse(CategoryMessage::UPDATE_CATE_SUCCESS, $category, 200);
        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
