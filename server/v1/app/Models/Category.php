<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class Category extends Model
{
    use HasFactory;
    protected $table='categories';
    public $timestamps = false;
    protected $fillable = [
        'code','name','slug','parent_id','status',
    ];

    public static function boot() {
        parent::boot();

        static::saving(function($model) {
            $model->user_created = JWTAuth::user()->id;
            $model->date_created = strtotime(date("d-m-Y H:i:s"));
        });

        static::updating(function($model) {
            $model->user_updated = JWTAuth::user()->id;
            $model->date_updated = strtotime(date("d-m-Y H:i:s"));
        });
    }

    public function parents() {
        return $this->children()->with('parents');
    }

    public function children() {
        return $this->hasMany(self::class,'parent_id');
    }
}
